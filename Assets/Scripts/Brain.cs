﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

[RequireComponent(typeof(ThirdPersonCharacter))]

public class Brain : MonoBehaviour {

	public int DNALength=1;
	public float timeAlive;
	public Dna dna;
	private ThirdPersonCharacter mCharacter;
	private Vector3 mMove;
	private bool mJump;
	bool alive=true;
	public float distanceTravelled=0;
	Vector3 startPos;
	void OnCollisionEnter(Collision obj){
		if (obj.gameObject.tag == "dead") {
			alive = false;
			distanceTravelled = 0;
		}
	}
	public void Init(){
		dna = new Dna (DNALength, 6);
		mCharacter = GetComponent<ThirdPersonCharacter> ();
		timeAlive = 0;
		alive = true;
		startPos = gameObject.transform.position;
	}

	
	// Update is called once per frame
	void FixedUpdate () {
		float h = 0;
		float v = 0;
		bool crouch = false;
		if (dna.GetGene (0) == 0)
			v = 1;
		else if (dna.GetGene (0) == 1)
			v = -1;
		else if (dna.GetGene (0) == 2)
			h = -1;
		else if (dna.GetGene (0) == 3)
			h = 1;
		else if (dna.GetGene (0) == 4)
			mJump = true;
		else if (dna.GetGene (0) == 5)
			crouch = true;
	

		mMove = v * Vector3.forward + h * Vector3.right;
		mCharacter.Move (mMove, crouch, mJump);
		mJump = false;
		if (alive) {
			timeAlive += Time.deltaTime;
			distanceTravelled = (transform.position - startPos).magnitude;

		}
			
	}
}
